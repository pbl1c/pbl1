package pbl1;

import java.util.*;
import java.lang.*;
import java.io.*;
import java.text.*;

public class CurrentTime {

  String s_month="";
  String lastTime="no"; //下校時間

  int Code1;
  int Code2;
  int Code3;



public String nowMonth(){ //現在の月を英語で返すメソッド(テーブルを検索するのに使う)

  Calendar calendar = Calendar.getInstance();
  int month = calendar.get(Calendar.MONTH);

  if(month==0){ 	//月を取得
    s_month="junuary";
  }else if(month==1){
    s_month="february";
  }else if(month==2){
    s_month="march";
  }else if(month==3){
    s_month="april";
  }else if(month==4){
    s_month="may";
  }else if(month==5){
    s_month="june";
  }else if(month==6){
    s_month="july";
  }else if(month==7){
    s_month="august";
  }else if(month==8){
    s_month="september";
  }else if(month==9){
    s_month="october";
  }else if(month==10){
    s_month="november";
  }else{
    s_month="december";
  }

  return s_month;

} //nowMonth


public String nowDay(){ //現在の時間を返すメソッド

  Calendar calendar = Calendar.getInstance();
  String day = String.valueOf(calendar.get(Calendar.DATE));
  return day;

} //nowTime



public String nowAD(){ //現在の西暦を yyyy 形式で返すメソッド

  Calendar cal = Calendar.getInstance();
  String ad="";
  SimpleDateFormat sdf = new SimpleDateFormat("yyyy");    //今の西暦を取得する
  ad = sdf.format(cal.getTime());
  return ad;

} //nowTime




public String nowTime(){ //現在の時間を返すメソッド

  Calendar calendar = Calendar.getInstance();
  String nowtime = (calendar.get(Calendar.HOUR_OF_DAY) ) + ":" + calendar.get(Calendar.MINUTE);
  return nowtime;

} //nowTime


public int math(){
  Calendar calendar = Calendar.getInstance();
  String math = String.valueOf((calendar.get(Calendar.HOUR_OF_DAY) ) *60+calendar.get(Calendar.MINUTE));
  int t = Integer.parseInt(math);
  return t;
}


public String Code1(){ //Code1を返すメソッド

  int t = math();
  System.out.println("現在の値"+t);

  if((t >=  480) && (t < 560)){   //8:00~9:20
    Code1 = 0;  //正常
  }else if((t >=  561) && (t < 575)){  //9:21 ~ 9:35
    Code1 = 1;  //遅刻
  }else if((t >=  576) && (t < 609)){  //9:36  ~10:9
    Code1 = 2;  //欠席
  }else if((t >=  610) && (t < 620)){  //10:10 ~ 10:20
    Code1 = 0;  //正常
  }else if((t >=  621) && (t < 635)){  //10:21 ~ 10:35
    Code1 = 1;  //遅刻
  }else if((t >=  636) && (t < 669)){ //10:36 ~ 11:09
    Code1 = 2;  //欠席
  }else if((t >=  670) && (t < 680)){ //11:10 ~ 11:20
    Code1 = 0;  //正常
  }else if((t >=  681) && (t < 695)){  //11:21 ~ 11:35
    Code1 = 1;  //遅刻
  }else if((t >=  696) && (t < 729)){  //11:36 ~ 12:09
    Code1 = 2;  //欠席
  }else if((t >=  730) && (t < 780)){ //12:10 ~ 13:00
    Code1 = 0;  //正常
  }else if((t >=  781) && (t < 795)){  //13:01 ~ 13:15
    Code1 = 1;  //遅刻
  }else if((t >=  796) && (t < 829)){ //13:16 ~ 13:49
    Code1 = 2;  //欠席
  }else if((t >=  830) && (t < 840)){ //13:50 ~ 14:00
    Code1 = 0;  //正常
  }else if((t >=  841) && (t < 855)){ //14:01 ~ 14:15
    Code1 = 1;  //遅刻
  }else if((t >=  856) && (t < 1080)){ //14:16 ~  18:00
    Code1 = 2;  //欠席
  }else{ //例外時間
    Code1 = 6;  //例外
  }
  String code = String.valueOf(Code1);
  System.out.println("CodeAは"+code);
  return code;
} // Code1









public String Code2(){ //Code2を返すメソッド

  int t = math();
  System.out.println("現在の値"+t);

  if((t >=  480) && (t < 560)){   //8:00~9:20
    Code2 = 1;    //1限目から
  }else if((t >=  561) && (t < 575)){  //9:21 ~ 9:35
    Code2 = 11;   //１限目遅刻
  }else if((t >=  576) && (t < 609)){  //9:36  ~10:9
    Code2 = 111;  //１限目欠席
  }else if((t >=  610) && (t < 620)){  //10:10 ~ 10:20
    Code2 = 2;    //２限目から出席
  }else if((t >=  621) && (t < 635)){  //10:21 ~ 10:35
    Code2 = 22;   //２限目遅刻
  }else if((t >=  636) && (t < 669)){ //10:36 ~ 11:09
    Code2 = 222;   //２限目欠席
  }else if((t >=  670) && (t < 680)){ //11:10 ~ 11:20
    Code2 = 3;     //３限目から出席
  }else if((t >=  681) && (t < 695)){  //11:21 ~ 11:35
    Code2 = 33;    //３限目遅刻
  }else if((t >=  696) && (t < 729)){  //11:36 ~ 12:09
    Code2 = 333;   //３限目欠席
  }else if((t >=  730) && (t < 780)){ //12:10 ~ 13:00
    Code2 = 4;     //４限目から出席
  }else if((t >=  781) && (t < 795)){  //13:01 ~ 13:15
    Code2 = 44;    //４限目遅刻
  }else if((t >=  796) && (t < 829)){ //13:16 ~ 13:49
    Code2 = 444;   //４限目欠席
  }else if((t >=  830) && (t < 840)){ //13:50 ~ 14:00
    Code2 = 5;     //５限目から出席
  }else if((t >=  841) && (t < 855)){ //14:01 ~ 14:15
    Code2 = 55;    //5限目遅刻
  }else if((t >=  856) && (t < 1080)){ //14:16 ~  18:00
    Code2 = 555;   //5限目欠席
  }else{ //例外時間
    Code2 = 6;     //例外
  }
  String code = String.valueOf(Code2);
  System.out.println("Code2は"+code);
  return code;
} //Code2


public String Code3(){ //Code3を返すメソッド

  int t = math();
  System.out.println("現在の値"+t);

  if((t >=  480) && (t < 609)){   //8:00~10:09
      Code3 = 0; //1限から欠席
    }else if((t >=  610) && (t < 669)){  //10:10 ~ 11:09
      Code3 = 1;//2限から欠席
    }else if((t >=  670) && (t < 729)){ //11:10 ~ 12:09
      Code3 = 2;//3限から欠席
    }else if((t >=  730) && (t < 829)){ //12:10 ~ 13:49
      Code3 = 3;//4限から欠席
    }else if((t >=  830) && (t < 855)){ //13:50 ~ 14:15
      Code3 = 4;//5限から欠席
    }else{ //例外時間
      Code3 = 6;//例外、欠席していない
    }

    String code = String.valueOf(Code3);
    System.out.println("Code3は"+code);
    return code;

} //Code3




public String Itizikanme(){ //１時間目を返すメソッド

  int t = math();
  String  s = "";

  if((t >=  480) && (t < 560)){   //8:00~9:20
    s = "出席"; //正常
  }else if((t >=  561) && (t < 575)){  //9:21 ~ 9:35
    s = "遅刻";  //遅刻
  }else if((t >=  576) && (t < 609)){  //9:36  ~10:9
    s = "欠席";  //欠席
  }else if((t >=  610) && (t < 620)){  //10:10 ~ 10:20
    s = "欠席";  //正常
  }else if((t >=  621) && (t < 635)){  //10:21 ~ 10:35
    s = "欠席";  //遅刻
  }else if((t >=  636) && (t < 669)){ //10:36 ~ 11:09
    s = "欠席";  //欠席
  }else if((t >=  670) && (t < 680)){ //11:10 ~ 11:20
    s = "欠席";  //正常
  }else if((t >=  681) && (t < 695)){  //11:21 ~ 11:35
    s = "欠席";  //遅刻
  }else if((t >=  696) && (t < 729)){  //11:36 ~ 12:09
    s = "欠席";  //欠席
  }else if((t >=  730) && (t < 780)){ //12:10 ~ 13:00
    s = "欠席";  //正常
  }else if((t >=  781) && (t < 795)){  //13:01 ~ 13:15
    s = "欠席";  //遅刻
  }else if((t >=  796) && (t < 829)){ //13:16 ~ 13:49
    s = "欠席";  //欠席
  }else if((t >=  830) && (t < 840)){ //13:50 ~ 14:00
    s = "欠席";  //正常
  }else if((t >=  841) && (t < 855)){ //14:01 ~ 14:15
    s = "欠席";  //遅刻
  }else if((t >=  856) && (t < 1080)){ //14:16 ~  18:00
    s = "欠席";  //欠席
  }else{ //例外時間
    s = "欠席";  //例外
  }

  System.out.println("1時間目は"+s);
  return s;

} //Itizikanme




public String Nizikanme(){ //１時間目を返すメソッド

  int t = math();
  String s;

  if((t >=  480) && (t < 560)){   //8:00~9:20
    s = "出席"; //正常 1
  }else if((t >=  561) && (t < 575)){  //9:21 ~ 9:35
    s = "出席";  //遅刻 1
  }else if((t >=  576) && (t < 609)){  //9:36  ~10:9
    s = "出席";  //欠席 1
  }else if((t >=  610) && (t < 620)){  //10:10 ~ 10:20
    s = "出席";  //正常 2
  }else if((t >=  621) && (t < 635)){  //10:21 ~ 10:35
    s = "遅刻";  //遅刻 2
  }else if((t >=  636) && (t < 669)){ //10:36 ~ 11:09
    s = "欠席";  //欠席 2
  }else if((t >=  670) && (t < 680)){ //11:10 ~ 11:20
    s = "欠席";  //正常 3
  }else if((t >=  681) && (t < 695)){  //11:21 ~ 11:35
    s = "欠席";  //遅刻 3
  }else if((t >=  696) && (t < 729)){  //11:36 ~ 12:09
    s = "欠席";  //欠席 3
  }else if((t >=  730) && (t < 780)){ //12:10 ~ 13:00
    s = "欠席";  //正常 4
  }else if((t >=  781) && (t < 795)){  //13:01 ~ 13:15
    s = "欠席";  //遅刻 4
  }else if((t >=  796) && (t < 829)){ //13:16 ~ 13:49
    s = "欠席";  //欠席 4
  }else if((t >=  830) && (t < 840)){ //13:50 ~ 14:00
    s = "欠席";  //正常 5
  }else if((t >=  841) && (t < 855)){ //14:01 ~ 14:15
    s = "欠席";  //遅刻 5
  }else if((t >=  856) && (t < 1080)){ //14:16 ~  18:00
    s = "欠席";  //欠席 5
  }else{ //例外時間
    s = "欠席";  //例外
  }

  System.out.println("2時間目は"+s);
  return s;

} //Nizikanme


public String Sanzikanme(){ //１時間目を返すメソッド

  int t = math();
  String s;

  if((t >=  480) && (t < 560)){   //8:00~9:20
    s = "出席"; //正常 1
  }else if((t >=  561) && (t < 575)){  //9:21 ~ 9:35
    s = "出席";  //遅刻 1
  }else if((t >=  576) && (t < 609)){  //9:36  ~10:9
    s = "出席";  //欠席 1
  }else if((t >=  610) && (t < 620)){  //10:10 ~ 10:20
    s = "出席";  //正常 2
  }else if((t >=  621) && (t < 635)){  //10:21 ~ 10:35
    s = "出席";  //遅刻 2
  }else if((t >=  636) && (t < 669)){ //10:36 ~ 11:09
    s = "出席";  //欠席 2
  }else if((t >=  670) && (t < 680)){ //11:10 ~ 11:20
    s = "出席";  //正常 3
  }else if((t >=  681) && (t < 695)){  //11:21 ~ 11:35
    s = "遅刻";  //遅刻 3
  }else if((t >=  696) && (t < 729)){  //11:36 ~ 12:09
    s = "欠席";  //欠席 3
  }else if((t >=  730) && (t < 780)){ //12:10 ~ 13:00
    s = "欠席";  //正常 4
  }else if((t >=  781) && (t < 795)){  //13:01 ~ 13:15
    s = "欠席";  //遅刻 4
  }else if((t >=  796) && (t < 829)){ //13:16 ~ 13:49
    s = "欠席";  //欠席 4
  }else if((t >=  830) && (t < 840)){ //13:50 ~ 14:00
    s = "欠席";  //正常 5
  }else if((t >=  841) && (t < 855)){ //14:01 ~ 14:15
    s = "欠席";  //遅刻 5
  }else if((t >=  856) && (t < 1080)){ //14:16 ~  18:00
    s = "欠席";  //欠席 5
  }else{ //例外時間
    s = "欠席";  //例外
  }

  System.out.println("3時間目は"+s);
  return s;

} //Sanzikanme




public String Yozikanme(){ //１時間目を返すメソッド

  int t = math();
  String s;

  if((t >=  480) && (t < 560)){   //8:00~9:20
    s = "出席"; //正常 1
  }else if((t >=  561) && (t < 575)){  //9:21 ~ 9:35
    s = "出席";  //遅刻 1
  }else if((t >=  576) && (t < 609)){  //9:36  ~10:9
    s = "出席";  //欠席 1
  }else if((t >=  610) && (t < 620)){  //10:10 ~ 10:20
    s = "出席";  //正常 2
  }else if((t >=  621) && (t < 635)){  //10:21 ~ 10:35
    s = "出席";  //遅刻 2
  }else if((t >=  636) && (t < 669)){ //10:36 ~ 11:09
    s = "出席";  //欠席 2
  }else if((t >=  670) && (t < 680)){ //11:10 ~ 11:20
    s = "出席";  //正常 3
  }else if((t >=  681) && (t < 695)){  //11:21 ~ 11:35
    s = "出席";  //遅刻 3
  }else if((t >=  696) && (t < 729)){  //11:36 ~ 12:09
    s = "出席";  //欠席 3
  }else if((t >=  730) && (t < 780)){ //12:10 ~ 13:00
    s = "出席";  //正常 4
  }else if((t >=  781) && (t < 795)){  //13:01 ~ 13:15
    s = "遅刻";  //遅刻 4
  }else if((t >=  796) && (t < 829)){ //13:16 ~ 13:49
    s = "欠席";  //欠席 4
  }else if((t >=  830) && (t < 840)){ //13:50 ~ 14:00
    s = "欠席";  //正常 5
  }else if((t >=  841) && (t < 855)){ //14:01 ~ 14:15
    s = "欠席";  //遅刻 5
  }else if((t >=  856) && (t < 1080)){ //14:16 ~  18:00
    s = "欠席";  //欠席 5
  }else{ //例外時間
    s = "欠席";  //例外
  }

  System.out.println("4時間目は"+s);
  return s;

} //Yozikanme





public String Gozikanme(){ //１時間目を返すメソッド

  int t = math();
  String s;

  if((t >=  480) && (t < 560)){   //8:00~9:20
    s = "出席"; //正常 1
  }else if((t >=  561) && (t < 575)){  //9:21 ~ 9:35
    s = "出席";  //遅刻 1
  }else if((t >=  576) && (t < 609)){  //9:36  ~10:9
    s = "出席";  //欠席 1
  }else if((t >=  610) && (t < 620)){  //10:10 ~ 10:20
    s = "出席";  //正常 2
  }else if((t >=  621) && (t < 635)){  //10:21 ~ 10:35
    s = "出席";  //遅刻 2
  }else if((t >=  636) && (t < 669)){ //10:36 ~ 11:09
    s = "出席";  //欠席 2
  }else if((t >=  670) && (t < 680)){ //11:10 ~ 11:20
    s = "出席";  //正常 3
  }else if((t >=  681) && (t < 695)){  //11:21 ~ 11:35
    s = "出席";  //遅刻 3
  }else if((t >=  696) && (t < 729)){  //11:36 ~ 12:09
    s = "出席";  //欠席 3
  }else if((t >=  730) && (t < 780)){ //12:10 ~ 13:00
    s = "出席";  //正常 4
  }else if((t >=  781) && (t < 795)){  //13:01 ~ 13:15
    s = "出席";  //遅刻 4
  }else if((t >=  796) && (t < 829)){ //13:16 ~ 13:49
    s = "出席";  //欠席 4
  }else if((t >=  830) && (t < 840)){ //13:50 ~ 14:00
    s = "出席";  //正常 5
  }else if((t >=  841) && (t < 855)){ //14:01 ~ 14:15
    s = "遅刻";  //遅刻 5
  }else if((t >=  856) && (t < 1080)){ //14:16 ~  18:00
    s = "欠席";  //欠席 5
  }else{ //例外時間
    s = "欠席";  //例外
  }

  System.out.println("5時間目は"+s);
  return s;

} //Gozikanme



} //Time
