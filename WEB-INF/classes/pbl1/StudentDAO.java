package pbl1;

import pbl1.CurrentTime;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.util.*;
import java.lang.*;
import java.io.*;


public class StudentDAO {

	CurrentTime currentTime = new CurrentTime();

	Context ctx = null;
	DataSource ds = null;
	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	ResultSet rs1 = null;
	ResultSet rs2 = null;
	ResultSet rs3 = null;


	String name="";
	String lastTime="no"; //下校時間
	String codeA = currentTime.Code1();
	String codeB = currentTime.Code2();
	String codeC = currentTime.Code3();
	String day = currentTime.nowDay();
	String month= currentTime.nowMonth();
	String nowTime = currentTime.nowTime();
	String iti = currentTime.Itizikanme();
	String ni = currentTime.Nizikanme();
	String san = currentTime.Sanzikanme();
	String yon = currentTime.Yozikanme();
	String go = currentTime.Gozikanme();
	String nowAD = currentTime.nowAD();
	int t = currentTime.math();

	public StudentDAO(){

		System.out.println("今の時間="+ nowTime);
		System.out.println("1時間目は=" +iti);
		System.out.println("2時間目は=" +ni);
		System.out.println("3時間目は=" +san);
		System.out.println("4時間目は=" +yon);
		System.out.println("5時間目は=" +go);

		try{

			ctx = new InitialContext();
			ds = (DataSource) ctx.lookup( "java:comp/env/jdbc/pbl1" );
			con = ds.getConnection();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}//StudentDAO




		/**
		 * ログイン処理
		 * @param ログインID
		 * @param パスワード　
		 * @return　ログイン判定を行い遷移先のurlを返す
		 */
		public String loginDesicion( String _loginId , String _loginPassword) {

			System.out.println();

			String dispathcerURL = "login.html";
			String strSql ="";

			try {

				System.out.println("まず教員かどうかチェックする");
				strSql = "SELECT * FROM teacher WHERE id = ? ";
				ps = con.prepareStatement(strSql);	// 教員かチェックする
				ps.setString ( 1 , _loginId );
				rs = ps.executeQuery();

				if(!rs.next()){	// 教員じゃないので学生かチェックする

					System.out.println("教員ではなかった。学生かどうかチェックする。");
					strSql="SELECT * FROM student"+nowAD+ " WHERE id=?";
					ps = con.prepareStatement(strSql);
					ps.setString ( 1 , _loginId );
					rs2 = ps.executeQuery();

					if(!rs2.next()){	// 教員でも学生でもないのでlogin.htmlへ戻らせる

						System.out.println("教員でも学生でもなかった。");
						dispathcerURL = "login.html";

//学生の場合の処理-----------------------------------------------------------------------------------------------------------------------------------------------
					}else if( _loginPassword.equals(rs2.getString("password"))){	// 入力したpasswordが学生のものと一致した場合

						System.out.println("学生で認識しました。");
						name = rs2.getString("name");

						if( _loginId.equals(_loginPassword)){	//初回ログインの場合

							System.out.println("初回ログインなのでパスワードを変更させる");
							dispathcerURL = "first_change_password.html";

						}else{	//初回以降の学生ログインの場合

							System.out.println("学生情報をSELECTする");
							System.out.println("SELECTする_loginId = " + _loginId);
							System.out.println("SELECTする day     = " + day);
							dispathcerURL = "index_s.jsp";

							strSql ="SELECT * FROM "+month+nowAD+" WHERE id=? AND day=? ";
							ps = con.prepareStatement(strSql);
							ps.setString ( 1 , _loginId );
							ps.setString ( 2 , day );
							rs3 = ps.executeQuery();

							System.out.println("SELECT完了");

						}


						if( !rs3.next() ){ //1回目loginの場合

							System.out.println("未登録なのでINSERTする");
							codeC="6";
							dispathcerURL = "index_s.jsp";	//index_s.jspに遷移する
							strSql = "INSERT INTO "+month+nowAD+" (id,name,day,time1,time2,code1,code2,code3,t1,t2,t3,t4,t5) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";

							ps = con.prepareStatement(strSql);
							ps.setString ( 1 , _loginId );
							ps.setString ( 2 , name);
							ps.setString ( 3 , day );
							ps.setString ( 4 , nowTime );
							ps.setString ( 5 , lastTime );
							ps.setString ( 6 , codeA );
							ps.setString ( 7 , codeB );
							ps.setString ( 8 , codeC );
							ps.setString ( 9 , iti );
							ps.setString ( 10 , ni );
							ps.setString ( 11 , san );
							ps.setString ( 12 , yon );
							ps.setString ( 13 , go );
							ps.executeUpdate();

							System.out.println("最初のINSERT完了");
							System.out.println("-----------------------------------------------------------------------------------------------------------------------------");


						}else{	//2回目のloginの場合

							System.out.println("登録済みなのでUPDATEする");
							dispathcerURL = "index_s.jsp";	//index_s.jspに遷移する

							if((t >=  480) && (t < 560)){   //8:00~9:20
						    strSql ="UPDATE "+month+nowAD+ " SET time2=?,code3=?,t1='欠席',t2='欠席',t3='欠席',t4='欠席',t5='欠席' WHERE id=? AND day=? ";  //正常 1
						  }else if((t >=  561) && (t < 575)){  //9:21 ~ 9:35
						    strSql ="UPDATE "+month+nowAD+ " SET time2=?,code3=?,t1='欠席',t2='欠席',t3='欠席',t4='欠席',t5='欠席' WHERE id=? AND day=? ";  //遅刻 1
						  }else if((t >=  576) && (t < 609)){  //9:36  ~10:9
						    strSql ="UPDATE "+month+nowAD+ " SET time2=?,code3=?,t1='欠席',t2='欠席',t3='欠席',t4='欠席',t5='欠席' WHERE id=? AND day=? ";  //欠席 1
						  }else if((t >=  610) && (t < 620)){  //10:10 ~ 10:20
						    strSql ="UPDATE "+month+nowAD+ " SET time2=?,code3=?,t2='欠席',t3='欠席',t4='欠席',t5='欠席' WHERE id=? AND day=? ";  //正常 2
						  }else if((t >=  621) && (t < 635)){  //10:21 ~ 10:35
						    strSql ="UPDATE "+month+nowAD+ " SET time2=?,code3=?,t2='欠席',t3='欠席',t4='欠席',t5='欠席' WHERE id=? AND day=? ";  //遅刻 2
						  }else if((t >=  636) && (t < 669)){ //10:36 ~ 11:09
						    strSql ="UPDATE "+month+nowAD+ " SET time2=?,code3=?,t2='欠席',t3='欠席',t4='欠席',t5='欠席' WHERE id=? AND day=? ";  //欠席 2
						  }else if((t >=  670) && (t < 680)){ //11:10 ~ 11:20
						    strSql ="UPDATE "+month+nowAD+ " SET time2=?,code3=?,t3='欠席',t4='欠席',t5='欠席' WHERE id=? AND day=? ";  //正常 3
						  }else if((t >=  681) && (t < 695)){  //11:21 ~ 11:35
						    strSql ="UPDATE "+month+nowAD+ " SET time2=?,code3=?,t3='欠席',t4='欠席',t5='欠席' WHERE id=? AND day=? ";  //遅刻 3
						  }else if((t >=  696) && (t < 729)){  //11:36 ~ 12:09
						    strSql ="UPDATE "+month+nowAD+ " SET time2=?,code3=?,t3='欠席',t4='欠席',t5='欠席' WHERE id=? AND day=? ";  //欠席 3
						  }else if((t >=  730) && (t < 780)){ //12:10 ~ 13:00
						    strSql ="UPDATE "+month+nowAD+ " SET time2=?,code3=?,t4='欠席',t5='欠席' WHERE id=? AND day=? ";  //正常 4
						  }else if((t >=  781) && (t < 795)){  //13:01 ~ 13:15
						    strSql ="UPDATE "+month+nowAD+ " SET time2=?,code3=?,t4='欠席',t5='欠席' WHERE id=? AND day=? ";  //遅刻 4
						  }else if((t >=  796) && (t < 829)){ //13:16 ~ 13:49
						    strSql ="UPDATE "+month+nowAD+ " SET time2=?,code3=?,t4='欠席',t5='欠席' WHERE id=? AND day=? ";  //欠席 4
						  }else if((t >=  830) && (t < 840)){ //13:50 ~ 14:00
						    strSql ="UPDATE "+month+nowAD+ " SET time2=?,code3=?,t5='欠席' WHERE id=? AND day=? ";  //正常 5
						  }else if((t >=  841) && (t < 855)){ //14:01 ~ 14:15
						    strSql ="UPDATE "+month+nowAD+ " SET time2=?,code3=?,t5='欠席' WHERE id=? AND day=? ";  //遅刻 5
						  }else if((t >=  856) && (t < 1080)){ //14:16 ~  18:00
						    strSql ="UPDATE "+month+nowAD+ " SET time2=?,code3=?,t5='欠席' WHERE id=? AND day=? ";  //欠席5
						  }else{ //例外時間
						    strSql ="UPDATE "+month+nowAD+ " SET time2=?,code3=?,t1='欠席',t2='欠席',t3='欠席',t4='欠席',t5='欠席' WHERE id=? AND day=? ";  //例外
						  }

							System.out.println("いまから実行するSQL文は"+strSql);

							ps = con.prepareStatement(strSql);
							ps.setString ( 1 , nowTime );
							ps.setString ( 2 , codeC );
							ps.setString ( 3 , _loginId);
							ps.setString ( 4 , day);
							ps.executeUpdate();

							System.out.println("UPDATE完了");
							System.out.println("-----------------------------------------------------------------------------------------------------------------------------");
							System.out.println("URLは"+dispathcerURL);

						}

					}else{		//学生情報と一致しなかったのでlogin.htmlに遷移させる

						dispathcerURL = "login.html";

					}
//学生の場合の処理終わり-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


			}else if( _loginPassword.equals(rs.getString("password"))){	// 入力したpasswordが教員のパスワードと一致したので教員用画面へ遷移する

					if( _loginId.equals(_loginPassword)){

						System.out.println("初回ログインなのでパスワード変更画面へ遷移");
						dispathcerURL = "first_change_password.html";

					}else{ //初回以降の教員ログイン

						dispathcerURL = "index.jsp";

					}

			}else{	// どの情報とも一致しなかったためlogin.htmlに遷移させる

						dispathcerURL = "login.html";

			}




			} catch(SQLException e) {
				e.printStackTrace();
			} catch(Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (rs != null)  rs.close();
					if (ps != null)  ps.close();
					if (con != null) con.close();
				} catch(SQLException e){
					e.printStackTrace();
				}
			}//finally

				System.out.println("遷移先は"+dispathcerURL);
				System.out.println("------------------------------------------------------------------------------------");
				return dispathcerURL; //指定されたURLに飛ぶ

		}//loginDesicion






		/**
		 * パスワードの変更を行う
		 * @param ログインid
		 * @param 新パスワード
		 * @param 現在のパスワード
		 */
		public void  updatePassword ( String _loginId , String _newPassword , String _currentPassword ){

			String strSql;

			if(_loginId.equals("teacher")){
				//教員の場合
				strSql ="UPDATE teacher SET password = ? WHERE id = ? AND password = ?";
			}else{
				//生徒の場合
				strSql ="UPDATE student" +nowAD+ " SET password = ? WHERE id = ? AND password = ?";
			}try{

				ps = con.prepareStatement(strSql);
				ps.setString ( 1 , _newPassword );
				ps.setString ( 2 , _loginId );
				ps.setString ( 3 , _currentPassword );
				ps.executeUpdate();

			} catch(SQLException e) {
				e.printStackTrace();
			} catch(Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (rs != null)  rs.close();
					if (ps != null)  ps.close();
					if (con != null) con.close();
				} catch(SQLException e){
					e.printStackTrace();
				}
			}//finally

		}//updatePassword




		/**
		 * code1の変更を行う
		 * @param ログインid
		 * @param 新パスワード
		 * @param 現在のパスワード
		 */
		public void  updateCode1 ( String _id , String _reason , String _reason1 , String _reason2 ,String _reason3 , String _reason4 , String _month, String _day ){

			System.out.println(_id);
			System.out.println(_reason);
			System.out.println(_reason1);
			System.out.println(_reason2);
			System.out.println(_reason3);
			System.out.println(_reason4);
			System.out.println(_month);
			System.out.println(_day);

			String strSql = "UPDATE "+_month+nowAD+" SET t1=?,t2=?,t3=?,t4=?,t5=? WHERE id=? AND day=?";


			try{

				ps = con.prepareStatement(strSql);
				ps.setString ( 1 , _reason );
				ps.setString ( 2 , _reason1 );
				ps.setString ( 3 , _reason2 );
				ps.setString ( 4 , _reason3 );
				ps.setString ( 5 , _reason4 );
				ps.setString ( 6 , _id );
				ps.setString ( 7 , _day );
				ps.executeUpdate();

			} catch(SQLException e) {
				e.printStackTrace();
			} catch(Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (rs != null)  rs.close();
					if (ps != null)  ps.close();
					if (con != null) con.close();
				} catch(SQLException e){
					e.printStackTrace();
				}
			}//finally

		}//updatePassword



		  /**
		   * パスワードのリセットを行う/神
		   * @param ログインid
		   * @param リセットパス
		   */
		  public void  reset_Pass ( String _studentId ){

		    String strSql;
		    strSql ="UPDATE student" +nowAD+ " SET password = ? WHERE id = ?";

		    try{

		      ps = con.prepareStatement(strSql);
		      ps.setString ( 1 , _studentId );
		      ps.setString ( 2 , _studentId );
		      ps.executeUpdate();

		    } catch(SQLException e) {
		      e.printStackTrace();
		    } catch(Exception e) {
		      e.printStackTrace();
		    } finally {
		      try {
		        if (rs != null)  rs.close();
		        if (ps != null)  ps.close();
		        if (con != null) con.close();
		      } catch(SQLException e){
		        e.printStackTrace();
		      }
		    }//finally
			 }//reset_Password










		/**
		 * 選択された学生情報の抽出を行う
		 * @param 生徒の名前
		 * @param 月
		 * @param 日
		 */
		public List<String> SearchStudent( String name , String _month , String _day ){

			System.out.println("生徒を探す");
			List<String> student = new ArrayList<String>();
			String strSql ="SELECT * FROM "+_month+nowAD+" WHERE name = ? AND day = ?";

			try {

				System.out.println("monthは"+ _month);
				System.out.println("dayは"+ _day);
				ps = con.prepareStatement(strSql);
				ps.setString ( 1 , name );
				ps.setString ( 2 , _day);
				rs = ps.executeQuery();

				if(!rs.next()){
					System.out.println("該当データなし");
				}else{
					System.out.println("データ発見");
						student.add(rs.getString("id"));
						student.add(rs.getString("name"));
						student.add(rs.getString("code1"));
						student.add(rs.getString("code2"));
						student.add(rs.getString("code3"));
				}

			} catch(SQLException e) {
				e.printStackTrace();
			} catch(Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (rs != null)  rs.close();
					if (ps != null)  ps.close();
					if (con != null) con.close();
				} catch(SQLException e){
					e.printStackTrace();
				}
			}//finally
			return student;
		}//SearchStudent




		public String GetStudentName(String _id){

			String name="";

		  try{

		    String strSql = "SELECT name FROM student"+nowAD+" WHERE id=?";
		    ps = con.prepareStatement(strSql);
				ps.setString(1,_id);
		    rs = ps.executeQuery();

				System.out.println("今からGetStudentNameを実行する");

				if(!rs.next()){
					System.out.println("該当データなし");
				}else{
					System.out.println("データ発見");
					name = rs.getString("name");
					System.out.println(name);
				}


		  } catch(SQLException e) {
		    e.printStackTrace();
		  } catch(Exception e) {
		    e.printStackTrace();
		  } finally {
		    try {
		      if (rs != null)  rs.close();
		      if (ps != null)  ps.close();
		      if (con != null) con.close();
		    } catch(SQLException e){
		      e.printStackTrace();
		    }
		  }//finally
		  return name;
		}// GetStudentName




		public void CreateCurrentMonthTable(){

			List<String> month = new ArrayList<String>();
			month.add("junuary");
			month.add("february");
			month.add("march");
			month.add("april");
			month.add("may");
			month.add("june");
			month.add("july");
			month.add("august");
			month.add("september");
			month.add("october");
			month.add("november");
			month.add("december");

			String strSql = "";

			try{

				for(int i=0; i<11 ; i++){

				strSql = "DROP TABLE IF EXISTS "+month.get(i)+nowAD;
				ps = con.prepareStatement(strSql);
				ps.executeUpdate();

		    strSql = "CREATE TABLE " +month.get(i)+nowAD+"(id varchar(7) NOT NULL, name varchar(20), day varchar(2), time1 varchar(5), time2 varchar(5), code1 varchar(1), code2 varchar(3), code3 varchar(1), t1 varchar(10), t2 varchar(10), t3 varchar(10), t4 varchar(10), t5 varchar(10)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		    ps = con.prepareStatement(strSql);
		    ps.executeUpdate();

			} //for文

		  } catch(SQLException e) {
		    e.printStackTrace();
		  } catch(Exception e) {
		    e.printStackTrace();
		  } finally {
		    try {
		      if (rs != null)  rs.close();
		      if (ps != null)  ps.close();
		      if (con != null) con.close();
		    } catch(SQLException e){
		      e.printStackTrace();
		    }
		  }//finally

		} // CreateCurrentMonthTable






		public void CreateCurrentStudentTable(){	//今年の学生のテーブルを作成する関数

			List<String> month = new ArrayList<String>();
			month.add("junuary");	//0
			month.add("february");	//1
			month.add("march");	//2
			month.add("april");	//3
			month.add("may");		//4
			month.add("june");	//5
			month.add("july");	//6
			month.add("august");	//7
			month.add("september");	//8
			month.add("october");	//9
			month.add("november");	//10
			month.add("december");	//11

			String strSql = "";

				try{

					for(int i=0; i<11 ; i++){

					strSql = "DROP TABLE IF EXISTS "+month.get(i)+nowAD;
					ps = con.prepareStatement(strSql);
					ps.executeUpdate();

					strSql = "CREATE TABLE " +month.get(i)+nowAD+"(id varchar(7) NOT NULL, name varchar(20), day varchar(2), time1 varchar(5), time2 varchar(5), code1 varchar(1), code2 varchar(3), code3 varchar(1), t1 varchar(10), t2 varchar(10), t3 varchar(10), t4 varchar(10), t5 varchar(10)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
					ps = con.prepareStatement(strSql);
					ps.executeUpdate();

				} //for文



				strSql = "DROP TABLE IF EXISTS student"+nowAD;
				ps = con.prepareStatement(strSql);
				ps.executeUpdate();

		    strSql = "CREATE TABLE student"+nowAD+"(id varchar(7) NOT NULL, password varchar(10), name varchar(20)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		    ps = con.prepareStatement(strSql);
		    ps.executeUpdate();


		  } catch(SQLException e) {
		    e.printStackTrace();
		  } catch(Exception e) {
		    e.printStackTrace();
		  } finally {
		    try {
		      if (rs != null)  rs.close();
		      if (ps != null)  ps.close();
		      if (con != null) con.close();
		    } catch(SQLException e){
		      e.printStackTrace();
		    }
		  }//finally

		} // CreateCurrentStudentTable


}//class StudentDAO
