package pbl1;

import pbl1.StudentDAO;
import pbl1.CurrentTime;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.io.FileReader;
import java.io.BufferedReader;
import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import javax.naming.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.text.*;
import pbl1.Pbl1Controller;

public class UploadFile extends HttpServlet {

        Context ctx = null;     // DataSourceをネーミングサービスで管理するオブジェクト
        DataSource ds = null;   // コネクションプーリングを管理しているオブジェクト
        Connection con = null;  // JavaとDBとを結ぶコネクション・オブジェクト
        PreparedStatement ps = null; //SQL文を操作するプリペアードステートメント・オブジェクト
        ResultSet rs = null;        //セレクト（抽出）したデータの集合体を表すリザルトセット・オブジェクト
        String strSql = "";

        CurrentTime currentTime = new CurrentTime();
        String ad = currentTime.nowAD();

    public void doPost( HttpServletRequest request  , HttpServletResponse response) throws ServletException , IOException{

        request.setCharacterEncoding ( "utf-8" );
        String tmp;
        StringTokenizer stk;
        // String ad;
        // Calendar cal = Calendar.getInstance();                  //西暦取得用にカレンダーを宣言
        // SimpleDateFormat sdf = new SimpleDateFormat("yyyy");    //今の西暦を取得する
        // ad = sdf.format(cal.getTime());
        // System.out.println("西暦は"+ad);


        Part csv = request.getPart("csv");  //CSV取得
        String name = null;

        // ファイル名を取り出す
        for(String disp : csv.getHeader("Content-Disposition").split(";")) {
            disp = disp.trim();
            if (disp.startsWith("filename")) {
                name = disp.substring(disp.indexOf("=") + 1).trim();
                name = name.replace("\"", "").replace("\\","/");
                int pos =name.lastIndexOf("/");
                if (pos >= 0) {
                    name = name.substring(pos + 1);
                   // System.out.println("nameは" + name);
                }
                break;
            }
        }
        // ファイル名を取り出した



        if(name != null){   // 正しくアップロードされたなら「WEB-INF/csvdata」フォルダに保存する
            String csvFile = this.getServletContext().getRealPath("/WEB-INF/csvdata") + "/" + name;
            csv.write(csvFile); //cscFile(パス)の中にstudent.csvを書き込み

            System.out.println("↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓");
            System.out.println("CSVのパスは" + csvFile);  //CSVファイルのパスを出力

            //ここから中身を取り出す
            //FileReader reCsv=new FileReader(csvFile); //CSV読み込み
            File file = new File(csvFile);
            FileInputStream input = new FileInputStream(file);  //Streamて何？
            InputStreamReader stream = new InputStreamReader(input,"SJIS"); //読み込んだファイルをSJISに変換する
            //BufferedReader brCsv = new BufferedReader(reCsv); //csvを開く
            BufferedReader brCsv = new BufferedReader(stream); //csvを開く

            try{

              StudentDAO StudentDAO = new StudentDAO();
              StudentDAO.CreateCurrentStudentTable(); //今年の学生のテーブルを作る関数を呼び出す
              System.out.println("学生のテーブル作った");
              // StudentDAO.CreateCurrentMonthTable(); //今年の1~12月のテーブルをつくる関数を呼び出す
              // System.out.println("1~12月のテーブル作った");

              ctx = new InitialContext();                                    //InitialContextオブジェクト(DataSourceをネーミングサービスで管理するオブジェクト)を生成
              ds = (DataSource) ctx.lookup( "java:comp/env/jdbc/pbl1" );     //JINDIを利用しデータベース”jsp”のDataSouceオブジェクトを探し出す
              con = ds.getConnection();                                      //データベースとのConnecitonオブジェクトを取得する
              System.out.println("データベース繋いだ");


            while (brCsv.ready()) {
            tmp = brCsv.readLine();
            byte[] b = tmp.getBytes();  //tmpの内容をバイトに変換して読み込む
            tmp = new String(b,"UTF-8");  //読み込んだ内容をUTF-8に変換？


            System.out.println("tmpは"+tmp);

            String[] strAry = tmp.split(",",-1); //tmpの中身を , で区切って配列に入れている
            System.out.println("格納するidは"+strAry[0]);
            System.out.println("格納する名前は"+strAry[1]);
            System.out.println("格納するパスワードは"+strAry[2]);

            strSql = "INSERT INTO student" +ad+ " (id,name,password) values(?,?,?)";  //テーブルに学生情報を突っ込む
            ps = con.prepareStatement(strSql);
            ps.setString(1,strAry[0]);
            ps.setString(2,strAry[1]);
            ps.setString(3,strAry[2]);
            ps.executeUpdate();

            stk = new StringTokenizer(tmp, ","); //カンマ区切りで行を突っ込む


            while (stk.hasMoreTokens()) {         // ","がまだあれば次の","に移る
            System.out.println(stk.nextToken());  //ここでデータ一個取り出し
            }
            //","の結果　次の学生
          }
            brCsv.close();  //ここまで中身を取り出すプログラム


    } catch (Exception e) {
      e.printStackTrace();
    }finally {
    //メインの処理（try句）でエラーが発生した場合も確実にclose( )できるように
    //finally句で記述する利用済みのオブジェクトをいつまでも開放せずにいるとメモリーリークを引き起こす
      try {
        if (rs != null) rs.close();
        if (con != null) con.close();
        if (ps != null) ps.close();
      }catch(Exception e){
        System.out.println("finally block :" + e.getMessage());
      }
    } //finally
} //if文終わり





    }//doPost

}//class
