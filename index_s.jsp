<%--JSPのキャラセット--%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.*,java.lang.* , java.io.* , java.sql.* , javax.naming.*" %>
<%@ page import="javax.sql.DataSource" %>
<jsp:useBean id="StudentDAO" class="pbl1.StudentDAO" scope="session" />
<jsp:useBean id="StudentBean" class="pbl1.StudentBean" scope="session" />
<jsp:useBean id="CurrentTime" class="pbl1.CurrentTime" scope="session" />

<%--HTMLヘッダー--%>

<!DOCTYPE html>
<html >
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Teacher's Menu</title>

  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css'>
  <link rel='stylesheet prefetch' href='css/index.css'>
  <link href="css/index.css" rel="stylesheet" type="text/css"/>
  <link href="css/select.css" rel="stylesheet" type="text/css"/>
  <link href="css/button.css" rel="stylesheet" type="text/css"/>
  <link href="css/modal.css" rel="stylesheet" type="text/css"/>
  <link href="css/inputfile.css" rel="stylesheet" type="text/css"/>
  <link href="css/dekkaitintin.css" rel="stylesheet" type="text/css"/>
  <link href="css/tittyaitintin.css" rel="stylesheet" type="text/css"/>
  <link href="css/passchangeOMANKO.css" rel="stylesheet" type="text/css"/>
  <link href="css/Kokojaiyanpaipanda.css" rel="stylesheet" type="text/css"/>
  <link href="css/input date.css" rel="stylesheet" type="text/css"/>
  <link href="css/new modaldal.css" rel="stylesheet" type="text/css"/>

  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css" media="screen" />

  <style></style>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
  <script src="js/index.js"></script>
  <%-- <script type="text/javascript" src='<c:url value="/js/index.js"/>'></script> --%>

</head>

<body>
  <div class="ct" id="t1">
  <div class="ct" id="t2">
  <div class="ct" id="t3">
  <div class="ct" id="t4">
  <div class="ct" id="t5">
  <div class="ct" id="t6">

  <ul id="menu">
  <ul class='wrap'>

    <form method="POST" action="pbl1controller">

        <a href="#t1"><li class="icon fa fa-home" id="uno" name="home"></li></a>
        <br>
        <%--メニューのアイコンホバータグの設定--%>
        <a href="#t2"><li  class="icon fa fa-pencil-square-o" id="dos" name="skManege"><span>出欠確認</span></li></a>
        <a href="#t5"><li class="icon fa fa-key" id="cinco" name="changePass"><span>パス変更</span></li></a>
        <a href="login.html"><li class="icon fa fa-sign-out" id="prpr" name="logout"><span>ログアウト</span></li></a>

    </ul>
  </ul>

<%--ホーム--%>
        <div class="page" id="p1">
<section class="icon fa fa-graduation-cap">学生用メニュー
  <span class="hint"><br><br><font size="38">ようこそ</font>
    <br>
    <br>


<%
  request.setCharacterEncoding("UTF-8");
  String StudentId = StudentBean.getId();
  String StudentName = StudentDAO.GetStudentName(StudentId);
%>

    <font size="100"><%out.println(StudentName);%>さん</font>
</span>
</section>
        </div>

<%--出欠管理--%>
          <div class="page" id="p2">
<section class="icon fa fa-pencil-square-o">出欠管理
  <span class="hint">

    <br>

    <select class="classic" name="month">
      <option value="">Month</option>
      <option value="junuary">1</option>
      <option value="february">2</option>
      <option value="march">3</option>
      <option value="april">4</option>
      <option value="may">5</option>
      <option value="june">6</option>
      <option value="july">7</option>
      <option value="august">8</option>
      <option value="september">9</option>
      <option value="october">10</option>
      <option value="november">11</option>
      <option value="december">12</option>
    </select>

    <select class="classic" name="day">
      <option value="">Days</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
      <option value="8">8</option>
      <option value="9">9</option>
      <option value="10">10</option>
      <option value="11">11</option>
      <option value="12">12</option>
      <option value="13">13</option>
      <option value="14">14</option>
      <option value="15">15</option>
      <option value="16">16</option>
      <option value="17">17</option>
      <option value="18">19</option>
      <option value="19">19</option>
      <option value="20">20</option>
      <option value="21">21</option>
      <option value="22">22</option>
      <option value="23">23</option>
      <option value="24">24</option>
      <option value="25">25</option>
      <option value="26">26</option>
      <option value="27">27</option>
      <option value="28">28</option>
      <option value="29">29</option>
      <option value="30">30</option>
      <option value="31">31</option>
    </select>

    <select class="classic" name="id">
    <%

      Context ctx = null;
      DataSource ds = null;
      Connection con = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      String ad = CurrentTime.nowAD();

      try{

        ctx = new InitialContext();
        ds = (DataSource) ctx.lookup( "java:comp/env/jdbc/pbl1" );
        con = ds.getConnection();

        String strSql = "SELECT * FROM student"+ad+" WHERE id=?";
        ps = con.prepareStatement(strSql);
        ps.setString(1,StudentId);
        rs = ps.executeQuery();

        while(rs.next()){
          out.println("<option value="+rs.getString("id")+">"+rs.getString("id")+"  "+rs.getString("name")+"</option>");
        }


      }catch(SQLException e) {
        e.printStackTrace();
      } catch(Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if (rs != null)  rs.close();
          if (ps != null)  ps.close();
          if (con != null) con.close();
        } catch(SQLException e){
          e.printStackTrace();
        }
      }

    %>
    </select>

    <input type="hidden" name="process" value="search_s"></input>

    <div class="analb">
    <button type="submit" name="post">決定</button>
    </div>
  </form>
  </span>
</section>
          </div>

<%--登校日登録--%>
          <div class="page" id="p3">
<section class="icon fa fa-calendar">登校日登録
   <span class="hint">

     <div class="control-group">
       <label class="control control--radio">前期
         <input type="radio" name="radio" checked="checked"/>
         <div class="control__indicator"></div>
       </label>

       <label class="control control--radio">後期
         <input type="radio" name="radio" checked="checked"/>
         <div class="control__indicator"></div>
       </label>
     </div>

     <div class="control-group">
       <label class="control control--radio">始業日
         <input type="radio" name="radio2" checked="checked"/>
         <div class="control__indicator"></div>
       </label>

       <label class="control control--radio">終業日
         <input type="radio" name="radio2" checked="checked"/>
         <div class="control__indicator"></div>
       </label>
     </div>


<br></br>
<input type="date">
  <div class= analb>
  <button type="submit" value="登録" >登録</button>
  </div>

   </span>
 </section>
          </div>



<!-- 宮松（新年度登録） -->
          <div class="page" id="p4">
<section class="icon fa fa-users">新年度登録
   <span class="hint">
   <br>

  <!-- 機能追加部分-->
  <label id="largeFile" for="file">

      <%-- <form method="POST" enctype="multipart/form-data" action="uploadfile"> --%>
        <form method="POST" enctype="multipart/form-data" action="pbl1controller">
          <input type="hidden" name="process" value="file">

      <input type="file" id="file" name="csv" accept="text/comma-separated-values"><br>

      </input>
      </label>

      <div class= analb>
      <button type="submit" value="登録" >登録</button>
      </div>

      </form>

   </span>
</section>
      </div>

<%--パスワード変更機能--%>
          <div class="page" id="p5">
<section class="icon fa fa-key">パスワード変更
    <span class="hint">

      <div class="h111"></div>

      <div class="container">

      <div class="card">

    <form method="post" action="pbl1controller" id="password_form">

      <div class="input-container">

        <input type="password" name="current_password" pattern="^[a-zA-Z\d]{6,10}$" size="6" maxlength="10" id="{label}" required="required" />
        <label for="{label}">  現在のパスワード</label>

        <div class="bar"></div>
      </div>

      <div class="input-container">

        <input type="password" name="new_password1" pattern="^[a-zA-Z\d]{6,10}$" size="6" maxlength="10" id="{label}" required="required" />
        <label for="{label}">新しいパスワード</label>

        <div class="bar"></div>
      </div>

      <div class="input-container">

        <input type="password" name="new_password2" pattern="^[a-zA-Z\d]{6,10}$" size="6" maxlength="10" id="{label}" required="required" />
        <label for="{label}">パスワードの確認</label>

        <div class="bar"></div>
      </div>

      <input type="hidden" name="process" value="change_password">

      <div class="button-container">

        <button  name="change" onclick="checkPassword( this );" style="position:absolute; left:130px; top:400px;WIDTH: 200px;">

          <span>変更</span>

        </button>
      </div>

    </form>


      <div class="toggle"></div>
      </div>
      </div>
  </span>
</section>
                        </div>
<%--管理機能--%>

          <div class="page" id="p6">
<section class="icon fa fa-cogs">管理機能
  <span class="hint">
<ul class='kanbozia'>
    <div class="tinko">

    <%--パスワードリセット--%>

    <div class="modal">
    <input id="modal__trigger3" type="checkbox" />
    <label for="modal__trigger3">パスワードリセット</label>
    <div class="modal__overlay" role="dialog" aria-labelledby="modal__title" aria-describedby="modal_desc">
    <div class="modal__wrap">
    <label for="modal__trigger3">&#10006;</label>
    <h2 id="modal__title"></h2>
    <p id="modal__desc">

    <form method="POST" action="Reset_kakunin.jsp">

      <select name="id">
      <%

        try{

          ctx = new InitialContext();
          ds = (DataSource) ctx.lookup( "java:comp/env/jdbc/pbl1" );
          con = ds.getConnection();

          String strSql = "SELECT * FROM student";
          ps = con.prepareStatement(strSql);
          rs = ps.executeQuery();

          while(rs.next()){
            out.println("<option value=" + rs.getString("id") + "&" + rs.getString("name") + ">" + rs.getString("id") + " " + rs.getString("name") + "</option>");
          }


        }catch(SQLException e) {
          e.printStackTrace();
        } catch(Exception e) {
          e.printStackTrace();
        } finally {
          try {
            if (rs != null)  rs.close();
            if (ps != null)  ps.close();
            if (con != null) con.close();
          } catch(SQLException e){
            e.printStackTrace();
          }
        }

        out.println("</select>");
        out.println("<button type='submit' name='post'>確認</button>");

      %>
    </form>

    </p>
    </div>
    </div>
    </div>

    <br></br>

  <%--バックアップとリストア--%>

  <div class="modal">
  <input id="modal__trigger4" type="checkbox" />
  <label for="modal__trigger4">バックアップと復元</label>
  <div class="modal__overlay" role="dialog" aria-labelledby="modal__title" aria-describedby="modal_desc">
  <div class="modal__wrap">
  <label for="modal__trigger4">&#10006;</label>
  <h2 id="modal__title"></h2>
  <p id="modal__desc">機能追加部分</p>
  </div>
  </div>
  </div>


  </div>

  </ul>
  </span>
</section>

             </div>

<%--以下の /div は各ページごとの /div です--%>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
 </form>
 </body>
</html>

<script language="JavaScript" type="text/javascript">

  function checkPassword( input ){

    if( document.forms.password_form.new_password1.value != document.forms.password_form.new_password2.value ){
         input.setCustomValidity('パスワードが一致していません');
        return false;
    }

  }

</script>
