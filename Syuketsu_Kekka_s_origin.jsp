<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.* , java.lang.* , java.io.* , java.sql.* , javax.naming.*" %>
<%@ page import="javax.sql.DataSource" %>
<jsp:useBean id="Student" class="pbl1.StudentBean" scope="session" />
<jsp:useBean id="CurrentTime" class="pbl1.CurrentTime" scope="session" />

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Syuketsu_Kekka</title>
</head>
<body>

選択したデータは

<%

  request.setCharacterEncoding("UTF-8");
  String id = "0K01014"; //今は西本のidを使う。あとでこの部分を変える。
  String month = String.valueOf(request.getParameter("month"));
  String day = String.valueOf(request.getParameter("day"));
  // out.println(id);
  // out.println(month);
  // out.println(day);
  String code1;
  String code2;
  String code3;
  String time1="出席";
  String time2="出席";
  String time3="出席";
  String time4="出席";
  String time5="出席";
  String ad = CurrentTime.nowAD();

  Context ctx = null;
  DataSource ds = null;
  Connection con = null;
  PreparedStatement ps = null;
  ResultSet rs = null;

  if(day.equals("") || month.equals("")){

    out.println("不正です。ちゃんと入力してください。");

  }else{

  try{

    ctx = new InitialContext();
    ds = (DataSource) ctx.lookup( "java:comp/env/jdbc/pbl1" );
    con = ds.getConnection();

    String strSql = "SELECT * FROM "+month+ad+" WHERE id=? AND day=? ";
    ps = con.prepareStatement(strSql);
    ps.setString(1,id);
    ps.setString(2,day);
    rs = ps.executeQuery();

    if(!rs.next()){
      out.println("存在しません");
    }else{
      out.println("存在します");
      out.println("<br>");
      id=rs.getString("id");
      code1=rs.getString("code1");
      code2=rs.getString("code2");
      code3=rs.getString("code3");
      time1=rs.getString("t1");
      time2=rs.getString("t2");
      time3=rs.getString("t3");
      time4=rs.getString("t4");
      time5=rs.getString("t5");

    // out.println("IDは"+rs.getString("id"));
      // out.println("NAMEは"+rs.getString("name"));
      // out.println("Code1は"+code1);
      // out.println("Code2は"+code2);
      // out.println("Code3は"+code3);
      // out.println("<br>");


    //  if(code1.equals("1") && code2.equals("1")){ //9:20分までに登校した場合(正常処理)
    //
    // }else if(code2.equals("11")){   //1時間目から登校し、遅刻時間だった場合
    //   time1="遅刻";
    // }else if(code2.equals("111") || code2.equals("2")){  //1時間目から登校し、欠席時間だった場合
    //   time1="欠席";
    // }else if(code2.equals("22")){   //2時間目から登校し、遅刻時間だった場合
    //   time1="欠席";
    //   time2="遅刻";
    // }else if(code2.equals("222") || code2.equals("3")){  //2時間目から登校し、欠席時間だった場合
    //   time1="欠席";
    //   time2="欠席";
    // }else if(code2.equals("33")){   //3時間目から登校し、遅刻時間だった場合
    //   time1="欠席";
    //   time2="欠席";
    //   time3="遅刻";
    // }else if(code2.equals("333") || code2.equals("4")){  //3時間目から登校し、欠席時間だった場合
    //   time1="欠席";
    //   time2="欠席";
    //   time3="欠席";
    // }else if(code2.equals("44")){   //4時間目から登校し、遅刻時間だった場合
    //   time1="欠席";
    //   time2="欠席";
    //   time3="欠席";
    //   time4="遅刻";
    // }else if(code2.equals("444") || code2.equals("5")){  //4時間目から登校し、遅刻時間だった場合
    //   time1="欠席";
    //   time2="欠席";
    //   time3="欠席";
    //   time4="欠席";
    // }else if(code2.equals("55")){   //5時間目から登校し、遅刻時間だった場合
    //   time1="欠席";
    //   time2="欠席";
    //   time3="欠席";
    //   time4="欠席";
    //   time5="遅刻";
    // }else if(code2.equals("555")){   //5時間目から登校し、遅刻時間だった場合
    //   time1="欠席";
    //   time2="欠席";
    //   time3="欠席";
    //   time4="欠席";
    //   time5="欠席";
    // }else if(code2.equals("6")){     //code2が6だった場合の処理
    //   time1="欠席";
    //   time2="欠席";
    //   time3="欠席";
    //   time4="欠席";
    //   time5="欠席";
    // }


    out.println("IDは"+id);
    out.println("<br>");

    out.println(1);
    out.println(time1);
    out.println("<br>");

    out.println(2);
    out.println(time2);
    out.println("<br>");

    out.println(3);
    out.println(time3);
    out.println("<br>");

    out.println(4);
    out.println(time4);
    out.println("<br>");

    out.println(5);
    out.println(time5);
    out.println("<br>");


    }

  }catch(SQLException e) {
    e.printStackTrace();
    out.println("存在しません");
  } catch(Exception e) {
    e.printStackTrace();
    out.println("Exception");
  } finally {
    try {
      if (rs != null)  rs.close();
      if (ps != null)  ps.close();
      if (con != null) con.close();
    } catch(SQLException e){
      e.printStackTrace();
    }
  } //finally
}





%>

</body>
</html>
