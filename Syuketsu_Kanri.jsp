<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.*,java.lang.* , java.io.* , java.sql.* , javax.naming.*" %>
<%@ page import="javax.sql.DataSource" %>
<jsp:useBean id="CurrentTime" class="pbl1.CurrentTime" scope="session" />

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Syuketsu_Kanri</title>
</head>
<body>

<form method="POST" action="pbl1controller">

<select name="month">
  <option value="">--</option>
  <option value="junuary">1</option>
  <option value="february">2</option>
  <option value="march">3</option>
  <option value="april">4</option>
  <option value="may">5</option>
  <option value="june">6</option>
  <option value="july">7</option>
  <option value="august">8</option>
  <option value="september">9</option>
  <option value="october">10</option>
  <option value="november">11</option>
  <option value="december">12</option>
</select>

<select name="day">
  <option value="">--</option>
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="4">4</option>
  <option value="5">5</option>
  <option value="6">6</option>
  <option value="7">7</option>
  <option value="8">8</option>
  <option value="9">9</option>
  <option value="10">10</option>
  <option value="11">11</option>
  <option value="12">12</option>
  <option value="13">13</option>
  <option value="14">14</option>
  <option value="15">15</option>
  <option value="16">16</option>
  <option value="17">17</option>
  <option value="18">19</option>
  <option value="19">19</option>
  <option value="20">20</option>
  <option value="21">21</option>
  <option value="22">22</option>
  <option value="23">23</option>
  <option value="24">24</option>
  <option value="25">25</option>
  <option value="26">26</option>
  <option value="27">27</option>
  <option value="28">28</option>
  <option value="29">29</option>
  <option value="30">30</option>
  <option value="31">31</option>
</select>

<select name="id">
<%

  Context ctx = null;
  DataSource ds = null;
  Connection con = null;
  PreparedStatement ps = null;
  ResultSet rs = null;
  String ad = CurrentTime.nowAD();

  try{

    ctx = new InitialContext();
    ds = (DataSource) ctx.lookup( "java:comp/env/jdbc/pbl1" );
    con = ds.getConnection();

    String strSql = "SELECT * FROM student"+ad;
    ps = con.prepareStatement(strSql);
    rs = ps.executeQuery();

    while(rs.next()){
      out.println("<option value="+rs.getString("id")+">"+rs.getString("id")+"  "+rs.getString("name")+"</option>");
    }


  }catch(SQLException e) {
    e.printStackTrace();
  } catch(Exception e) {
    e.printStackTrace();
  } finally {
    try {
      if (rs != null)  rs.close();
      if (ps != null)  ps.close();
      if (con != null) con.close();
    } catch(SQLException e){
      e.printStackTrace();
    }
  }

%>
</select>

<input type="hidden" name="process" value="search">
<button type="submit" name="post">決定</button>

</form>
</body>
</html>
