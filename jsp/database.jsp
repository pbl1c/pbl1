<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.lang.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="javax.naming.Context" %>
<%@ page import="javax.naming.InitialContext" %>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>データベースの処理</title>
</head>
<body>

<%

Context ctx = null;
DataSource ds = null;
Connection con = null;
PreparedStatement ps = null;
ResultSet rs = null;
String strSql ="";


public String[] studentName(){

  String[] name = null;

  try{

    ctx = new InitialContext();
    ds = (DataSource) ctx.lookup( "java:comp/env/jdbc/pbl1" );
    con = ds.getConnection();

    strSql = "SELECT * FROM student";
    ps = con.prepareStatement(strSql);
    rs = ps.executeQuery();

    while(rs.next()){
      int i=0;
      name[i](rs.getString("name"));
      out.println(name[i]);
      i++;
    }
    return name;

} catch(SQLException e) {
  e.printStackTrace();
} catch(Exception e) {
  e.printStackTrace();
} finally {
  try {
    if (rs != null)  rs.close();
    if (ps != null)  ps.close();
    if (con != null) con.close();
  } catch(SQLException e){
    e.printStackTrace();
  }
}//finally

}
%>
</body>
</html>
